#ifndef PARALLELIZER_H
#define PARALLELIZER_H

#include <vector>
#include <string>
#include <stdexcept>

#include <rdi_stl_utils.h>

extern "C++"
{

namespace RDI {
namespace parallel {

inline std::vector<size_t> get_chunk_bounds(size_t input_size,
										 size_t number_of_chunks)
{
	if(input_size < number_of_chunks)
	{
		throw std::runtime_error("Input size is less than the number of threads");
	}

	size_t chunk_size = input_size / number_of_chunks;
	size_t reminder = input_size % number_of_chunks;

	std::vector<size_t> bounds;

	for (size_t i = 0; i <= (input_size - reminder); i += chunk_size)
	{
		bounds.push_back(i);
	}

	bounds[number_of_chunks] += reminder;
	return bounds;
}


template<typename T>
std::vector<std::vector<T>> split_to_chunks(std::vector<T> input,
											size_t number_of_chunk)
{
	if(input.size() < number_of_chunk)
	{
		throw std::runtime_error("Input size is less than the number of threads");
	}

	std::vector<size_t> bounds = get_chunk_bounds(input.size(), number_of_chunk);
	std::vector<std::vector<T>> chunks;

	for (size_t i = 0; i < number_of_chunk; ++i)
	{
		std::vector<T> tmp_chunk;
		size_t chunk_start = bounds[i];
		size_t chunk_end = bounds[i + 1];

		for(size_t j = chunk_start; j < chunk_end ;j++)
		{
			tmp_chunk.push_back( input[j] );
		}

		chunks.push_back(tmp_chunk);
	}
	return chunks;

}

template<typename T>
std::vector<T> combine_chunks(const std::vector<std::vector<T>> &chunks)
{
	std::vector<T> output_lines;

	for (size_t i = 0; i < chunks.size(); ++i)
	{
		output_lines = RDI::concat_vectors(output_lines, chunks[i]);
	}

	return output_lines;
}

inline size_t check_thread_numbers(size_t input_size, size_t num_threads)
{
	if(num_threads <= 0)
	{
		return 1;
	}
	else if(num_threads > input_size)
	{
		return input_size;
	}

	return num_threads;
}

} // namespace parallel
} // namespace RDI

} // extern "C++"

#endif // PARALLELIZER_H
