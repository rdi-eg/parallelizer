#-------------------------------------------------
#
# Project created by QtCreator 2018-07-22T15:40:32
#
#-------------------------------------------------

TARGET = parallelizer
TEMPLATE = lib
CONFIG += staticlib c++14
CONFIG -= qt

SOURCES +=

HEADERS += \
    rdi_parallelizer.h

INCLUDEPATH += $$PWD/../include
